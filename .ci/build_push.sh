
tee $DOCKERFILE <<-EOL
FROM node:current-alpine
RUN addgroup -S app && adduser -SDH -g "" -G app app
COPY code /code
RUN cd /code/api && npm run setup && cd /code/web && npm run setup
WORKDIR ${WORKDIR}
USER app
EXPOSE ${EXPOSE_PORT}
ENTRYPOINT ["npm", "start"]
EOL
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build --no-cache -t ${CI_REGISTRY}/sablin-otus-cicd-labs/crate/${IMAGE_NAME}:${VERSION} -f ${DOCKERFILE} .
docker push ${CI_REGISTRY}/sablin-otus-cicd-labs/crate/${IMAGE_NAME}:${VERSION}